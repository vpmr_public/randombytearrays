#ifndef RBA_PARSER
#define RBA_PARSER

#include "IModule.h"

class RbaParser: public IModule
{
public:
  RbaParser();
  virtual ~RbaParser() = default;

protected:
  void threadLoopFunction() override;

  void parse();
  bool match(byte_array_t& byteArray);
  bool match(byte_array_it_t dataIt, const byte_array_t& data);
};


#endif // RBA_PARSER