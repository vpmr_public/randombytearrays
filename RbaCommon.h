#ifndef RBA_COMMON
#define RBA_COMMON

#include <vector>
#include <deque>
#include <cstdint>
#include <memory>
#include <functional>
#include <mutex>
#include <iostream>

const int MAX_QUEUE_SIZE = 10000;
const int MAX_ARRAY_SIZE = 100;
const int APROX_EXECUTION_TIME_S = 100;

enum class IModuleType
{
  TYPE_PARSER,
  TYPE_GENERATOR,
  TYPE_PRINTER
};

using byte_t = uint8_t;
using byte_array_t = std::vector<byte_t>;

const byte_array_t BYTE_PATTERN{0x00, 0x01, 0x02};

using byte_array_it_t = byte_array_t::iterator;
using byte_array_cit_t = byte_array_t::const_iterator;
using byte_array_ptr_t = std::shared_ptr<byte_array_t>;
using get_byte_array_ptr_cb_t = std::function<byte_array_ptr_t(void)>;
using thread_loop_action_cb_t = std::function<void(void)>;

static void printArray(const byte_array_t& data, uint64_t num = 0u, uint64_t epoch_ms = 0u)
{
  std::cout << std::endl << "(·) ";
  if (num)
  {
    std::cout << num << " ";  
  }
  if (epoch_ms)
  {
    std::cout << "[" << epoch_ms << "] ";  
  }
  uint8_t count{0u};
  for (byte_array_cit_t it = data.begin(); it != data.end(); ++it)
  {
    std::cout << 1 + *it - 1 << "|";
  }
  std::cout << " ■" << std::endl;
}

static uint64_t getEpoch_ms()
{
   return std::chrono::duration_cast<std::chrono::milliseconds>
       (std::chrono::system_clock::now().time_since_epoch()).count();
}

class ByteDeque
{
public:
  ByteDeque() = default;
  virtual ~ByteDeque() = default;
  virtual bool push_back(const byte_array_ptr_t& value)
  {
  	std::lock_guard<std::mutex> lg(mMutex);
  	mDeque.push_back(value);
    return true;
  }
  virtual bool push_back(byte_array_ptr_t&& value)
  {
  	std::lock_guard<std::mutex> lg(mMutex);
  	mDeque.push_back(value);
    return true;
  }
  virtual byte_array_ptr_t pop_front()
  {
  	std::lock_guard<std::mutex> lg(mMutex);
  	byte_array_ptr_t front = nullptr;
  	if (!mDeque.empty())
    {
      front = mDeque.front();
   	  mDeque.pop_front();
    }
  	return front;
  }
  virtual size_t size()
  {
    std::lock_guard<std::mutex> lg(mMutex);
  	return mDeque.size();
  }
  virtual void printAll()
  {
    std::lock_guard<std::mutex> lg(mMutex);
    uint64_t num = 1u;
  	for (auto it = mDeque.begin(); it != mDeque.end(); ++it, ++num)
  	{
  	  if (*it)
  	  {
        printArray(**it, num);
  	  }
  	}
  }
  
protected:
  std::deque<byte_array_ptr_t> mDeque;
  std::mutex mMutex;
};

class TimestampedByteDeque
{
public:
  TimestampedByteDeque(size_t size = MAX_QUEUE_SIZE): mCapacity(size)
  {
    mVector.reserve(mCapacity);
  }
  virtual ~TimestampedByteDeque() = default;
  virtual bool push_back(byte_array_ptr_t value)
  {
    std::lock_guard<std::mutex> lg(mMutex);
    auto ret{true};
    if (mCount < mCapacity)
    {
      std::pair<uint64_t, byte_array_ptr_t> pair{getEpoch_ms(), value};
      mVector[mNextInsertionIndex++] = pair;
      ++mCount;
      if (mNextInsertionIndex == mCapacity)
      {
        mNextInsertionIndex = 0u;
      }
    }
    else
    {
      ret = false;
    }
    return ret;
  }
  virtual byte_array_ptr_t pop_front()
  {
    std::lock_guard<std::mutex> lg(mMutex);
    byte_array_ptr_t front = nullptr;
    if (mCount > 0u)
    {
      front = mVector[mNextTakeIndex].second;
      mVector[mNextTakeIndex++].second = nullptr;
      --mCount;
      if (mNextTakeIndex == mCapacity)
      {
        mNextTakeIndex = 0u;
      }
    }
    return front;
  }
  virtual size_t size()
  {
    std::lock_guard<std::mutex> lg(mMutex);
    return mCount;
  }
  virtual size_t fullPercent()
  {
    std::lock_guard<std::mutex> lg(mMutex);
    return 100*mCount/mCapacity;
  }
  virtual void printAll()
  {
    std::lock_guard<std::mutex> lg(mMutex);
    uint64_t num = 1u;
    for (auto idx = mNextTakeIndex; idx != mNextInsertionIndex && num <= mCount; ++idx, ++num)
    {
      if (idx == mCapacity)
      {
        idx = 0u;
      }
      auto& elem = mVector[idx];
      if (elem.second)
      {
        printArray(*elem.second, num, elem.first);
      }
    }
  }
  
protected:

  std::vector<std::pair<uint64_t, byte_array_ptr_t>> mVector;
  std::mutex mMutex;
  size_t mCapacity{0u};
  size_t mNextTakeIndex{0u};
  size_t mNextInsertionIndex{0u};
  size_t mCount{0u};
};

using queue_t = TimestampedByteDeque;

#endif // RBA_COMMON