#include "RbaPrinter.h"
#include <iostream>

RbaPrinter::RbaPrinter()
{
  mName = "PRINTER";
}

void RbaPrinter::threadLoopFunction()
{
  printIfNew();
}

void RbaPrinter::threadExitFunction()
{
  mData.printAll();
}

void RbaPrinter::printIfNew()
{
  if (isFullQueue())
  {
    return;
  }
  if (mGetData)
  {
    auto byteArray = mGetData();
    if (byteArray)
    {
      mData.push_back(byteArray);
      ++mCounter;
      mData.printAll();
    }
  }
}