#ifndef RBA_FACTORY
#define RBA_FACTORY

#include "IModule.h"

struct RbaFactory
{
  static IModulePtr generateIModule(IModuleType type);
};


#endif // RBA_FACTORY