#include "RbaFactory.h"

#include "RbaParser.h"
#include "RbaGenerator.h"
#include "RbaPrinter.h"


struct RbaFactoryPriv
{
  static IModulePtr generateRbaParser()
  {
     return std::make_shared<RbaParser>();
  }
  static IModulePtr generateRbaGenerator()
  {
     return std::make_shared<RbaGenerator>();
  }
  static IModulePtr generateRbaPrinter()
  {
     return std::make_shared<RbaPrinter>();
  }
};

IModulePtr RbaFactory::generateIModule(IModuleType type)
{
  switch(type)
  {
    case IModuleType::TYPE_PARSER:    return RbaFactoryPriv::generateRbaParser();
    case IModuleType::TYPE_GENERATOR: return RbaFactoryPriv::generateRbaGenerator();
    case IModuleType::TYPE_PRINTER:   return RbaFactoryPriv::generateRbaPrinter();
    default: return nullptr;
  }
}