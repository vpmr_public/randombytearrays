#ifndef RBA_IMODULE
#define RBA_IMODULE

#include "RbaCommon.h"
#include <thread>
#include <atomic>


class IModule
{	
public:
  IModule() = default;
  virtual ~IModule() = default;

  virtual void start()
  {
  	mRunning = true;
  	mThread = std::thread(&IModule::threadLoop, this);
  }
  virtual void stop()
  {
  	mRunning = false;
  	if (mThread.joinable())
  	{
  		mThread.join();
  	}
  }
  virtual void setDataSource(get_byte_array_ptr_cb_t callback){ mGetData = callback; }
  virtual get_byte_array_ptr_cb_t getDataCallback(){ return [this](){return this->getData(); }; }

protected:
  virtual void threadLoop()
  {
    while(mRunning)
    {
      threadLoopFunction();
      std::this_thread::sleep_for(std::chrono::microseconds(mData.fullPercent()));
    }
    threadExitFunction();
    std::cout << "############################ END THREAD " << mName << ", TOTAL: " << mCounter << std::endl;
  }
  virtual void threadLoopFunction() = 0;
  virtual void threadExitFunction(){}
  virtual byte_array_ptr_t getData()
  {
    return mData.pop_front(); 
  }

  bool isFullQueue()
  {
    if(mData.size() >= MAX_QUEUE_SIZE)
    {
      if (!mFullQueue)
      {
        std::cout << std::endl << std::endl << "### " << mName << " QUEUE FULL!" << std::endl << std::endl;
        mFullQueue = true;
      }
    }
    else
    {
      if (mFullQueue)
      {
        std::cout << std::endl << std::endl << "@@@ " << mName << " QUEUE FREED!" << std::endl << std::endl;
        mFullQueue = false;
      }
    }
    return mFullQueue;
  }

  std::thread mThread;
  std::atomic_bool mRunning{false};
  get_byte_array_ptr_cb_t mGetData{nullptr};
  std::string mName{""};
  queue_t mData;
  uint64_t mCounter{0u};
  bool mFullQueue{false};
};

using IModulePtr = std::shared_ptr<IModule>;
#endif // RBA_IMODULE