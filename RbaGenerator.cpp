#include "RbaGenerator.h"

#include <stdlib.h>
#include <time.h>
#include <iostream>

RbaGenerator::RbaGenerator()
{
  mName = "GENERATOR";
  srand(time(NULL));
}

void RbaGenerator::threadLoopFunction()
{
  generate();
}

void RbaGenerator::generate()
{
  if (isFullQueue())
  {
    return;
  }

  auto byteArray = std::make_shared<byte_array_t>();  
  int max_size = (rand() % (MAX_ARRAY_SIZE-1)) + 1;
  byteArray->reserve(max_size);
  for(int index = 0; index < max_size; ++index)
  {
    mPenultimateRandom = mLastRandom;
    mLastRandom = rand();
    byteArray->emplace_back(mLastRandom % 255);
  }
  mData.push_back(byteArray);
  ++mCounter;
  mPenultimateRandom = -1;
}