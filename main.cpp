#include "RbaFactory.h"

#include <iostream>

#include <chrono>
using namespace std::chrono;

int main()
{

  auto generator = RbaFactory::generateIModule(IModuleType::TYPE_GENERATOR);
  auto parser    = RbaFactory::generateIModule(IModuleType::TYPE_PARSER);
  auto printer   = RbaFactory::generateIModule(IModuleType::TYPE_PRINTER);

  parser->setDataSource(generator->getDataCallback());
  printer->setDataSource(parser->getDataCallback());

  generator->start();
  parser->start();
  printer->start();

  int count{0};
  while(count++ < APROX_EXECUTION_TIME_S)
  {
    std::cout << "." << std::flush;
    std::this_thread::sleep_for (std::chrono::seconds(1));
  }

  generator->stop();
  parser->stop();
  printer->stop();
   
  return 0;
}