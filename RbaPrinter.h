#ifndef RBA_PRINTER
#define RBA_PRINTER

#include "IModule.h"

class RbaPrinter: public IModule
{
public:
  RbaPrinter();
  virtual ~RbaPrinter() = default;

protected:
  void threadLoopFunction() override;
  void threadExitFunction() override;
  
  void printIfNew();
};


#endif // RBA_PRINTER