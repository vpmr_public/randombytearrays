#ifndef RBA_GENERATOR
#define RBA_GENERATOR

#include "IModule.h"
#include <stdio.h>

class RbaGenerator: public IModule
{
public:
  RbaGenerator();
  virtual ~RbaGenerator() = default;

protected:
  void threadLoopFunction() override;
  
  void generate();
  
  int mLastRandom{-1};
  int mPenultimateRandom{-1};
  bool mFullQueue{false};
};


#endif // RBA_GENERATOR