#include "RbaParser.h"
#include <iostream>

RbaParser::RbaParser()
{
  mName = "PARSER";
}

void RbaParser::threadLoopFunction()
{
  parse();
}

void RbaParser::parse()
{
  if (isFullQueue())
  {
    return;
  }
  if (mGetData)
  {
    auto byteArray = mGetData();
    if (byteArray)
    {
      if (match(*byteArray))
      {
        mData.push_back(byteArray);
      }
      ++mCounter;
    }
  }
}

bool RbaParser::match(byte_array_t& byteArray)
{
  auto ret{false};
  auto patternBegin = BYTE_PATTERN.begin();
  for (byte_array_it_t it = byteArray.begin(); it != byteArray.end(); ++it)
  {  	
  	if (match(it, byteArray))
  	{
	  ret = true;
	  break;
  	}
  }
  return ret;
}

bool RbaParser::match(byte_array_it_t dataIt, const byte_array_t& data)
{
  auto patternIt = BYTE_PATTERN.begin();
  for (; dataIt != data.end() && patternIt != BYTE_PATTERN.end(); ++dataIt, ++patternIt)
  {
    if (!(*dataIt == *patternIt))
    {
      break;
    }
  }
  return patternIt == BYTE_PATTERN.end();
}